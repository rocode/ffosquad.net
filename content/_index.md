+++
+++

![FFO Logo](FFO_logo.svg)
[![Discord Logo](Discord_logo.svg)](https://discord.io/ffosquad)
[![Teamspeak Logo](TeamSpeak_logo.png)](ts3server://ts.ffosquad.net/%3Faddbookmark%3DFFO)


Server Rules:

1. No bigoted language


   Although we are usually concerned with the intent behind language rather than the use of any particular words, we make an exception for language which is primarily associated with hate speech towards a particular demographic. Regardless of intent, the cultural associations with these words makes their usage unconducive to a friendly environment. Depending on the severity of the infraction, you may be kicked, and or banned immediately.

2. Do not consciously hinder your team


   Examples of this include, but are not limited to, intentional teamkilling, building irrelevant superFOBs, sitting in main, stealing claimed vehicles etc. This is not about player skill; all skill levels are welcome as long as a player is making an honest effort to contribute to their team. This rule is an effort to facilitate teamplay.

3. Squad Leaders are required to have a microphone
   
   
   We support and encourage new Squad Leaders to step up and try the reigns. However, we do expect you to utilize your voice comms to better facilitate communication and teamwork on the team.

4. No Intentional Glitching, Cheating, or Hacking
   
   
   Intentional glitching will result in a kick, Cheating and Hacking will result in a ban, and will be promptly reported to the Squad Devs, with all available evidence.

5. Pretending to be FFO Member, Admin, or Mod
   
   
   This will result in an immediate ban, no warnings will be given.

6. Ban evasions of any kind will result in a permanent ban
   
   
   If you attempt to evade a ban regardless of the initial offence, you will receive a permanent ban. This includes disconnecting after breaking rules.

7. If the server is full you may be removed for being AFK
   
   
   This is in no way meant to be a punishment, this is just to make room for people who want to play. 

Squad Server Popping Rules: 

1. Fight over center objective 
2. Do not use vehicles until 20v20
3. Do not camp HABs until game is live 
4. Game is live at 18v18 or when the Admin calls live

Squad Vehicle Claiming Rules: 
1. The first squad with the name of the vehicle in their name gets claim of vehicle. 
   
   
   Example: Name your squad "Helo Squad." If you are the first to make a squad with that name of the vehicle you get claim of that vehicle.

